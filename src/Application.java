import java.util.Scanner;

public class Application {
    //ex1 - > mainul este pentru toate ex
    public static void main(String[] args) {
        System.out.println("Dati diametrul cercului pentru a calcula perimetrul");
        Scanner keyboard = new Scanner(System.in);

        float diam = keyboard.nextInt();


        diamePerim(diam);
        bodyMassIndex(keyboard);
    }

    public static void diamePerim(double diam) {
        double pi = Math.PI;
        double perim = pi * diam;
        System.out.println("Perimetrul cercului este " + perim);
    }

    //ex 2
    public static void bodyMassIndex(Scanner keyboardForMassIndex) {
        float weightKg = 0;
        float heightM = 0;
        int heightCm = 0;
        float bmi = 0;
        System.out.println("Introduceti greutatea in kg ");
        weightKg = keyboardForMassIndex.nextFloat();
        System.out.println("Introduceti inaltimea in cm");
        heightCm = keyboardForMassIndex.nextInt();
        heightM = heightCm / 100f;

        bmi = weightKg / (heightM * heightM);
        System.out.println("Indexul de masa corporala este :" + bmi);
        if (bmi > 18.5 && bmi < 24.9) {
            System.out.println("Userul este normal ponderat");
        }
        if (bmi < 18.5) {
            System.out.println("Userul este subponderat");
        }
        if (bmi > 24.9) {
            System.out.println("Userul este supraponderat");
        }
    }



}
